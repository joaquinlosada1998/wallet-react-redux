import React from 'react'
import Seccion from '../componentes/Seccion'

const Carrito = () => {
    return (
        <Seccion titulo="Carrito">
            <p>No tiene elementos en el carrito</p>
        </Seccion>
    )
}

export default Carrito
